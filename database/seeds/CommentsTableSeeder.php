<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
        	DB::table('comments')->insert([
        		'post_id' => $faker->numberBetween(1, 10),
        		'author_id' => $faker->numberBetween(1, 10),
        		'created_at' => $faker->dateTime(),
        		'updated_at' => $faker->dateTime(),
        		'text' => $faker->realText($faker->numberBetween(10, 40))
        	]);
        }
    }
}
