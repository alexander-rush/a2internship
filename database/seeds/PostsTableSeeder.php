<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

      for($i = 0; $i < 10; $i++) {
      	DB::table('posts')->insert([
      		'title' => $faker->realText($faker->numberBetween(10, 15)),
      		'text' => $faker->realText($faker->numberBetween(40, 50)),
      		'is_published' => $faker->boolean(50),
      		'author_id' => $faker->numberBetween(1, 10),
      		'created_at' => $faker->dateTime(),
      		'updated_at' => $faker->dateTime(),
      		'post_type' => $faker->numberBetween(1, 10)
      	]);
      }
    }
}
