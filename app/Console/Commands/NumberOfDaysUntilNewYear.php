<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class NumberOfDaysUntilNewYear extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'days_until_new_year:get {date}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Returns a number of days until New Year';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    try {
      $date = new Carbon($this->argument('date'));
      $year = $date->year + 1;
      echo $date->diffInDays(new Carbon("first day of January {$year}"));
    } catch(\Exception $e) {
      echo 'Invalid value.';
    }     
  }
}
